A simple, proof-of-concept 3D graphing program. Allows the user to enter a plain-text mathematical expression using the operators `+`, `-`, `*`, `/`, and `^`, along with several common functions. The expression is graphed approximately by dividing the plane into small triangles and evaluating the function at each vertex. In addition to graphing a function, the user may enter a number of tuples `(x, y, z)` and the corresponding points will be shown on the graph. The user can drag the mouse to rotate the graph, and use the scroll wheel to zoom in or out.

However, there is currently no way to move the frame away from the origin. Further, since the graphics computation is written from scratch and does not make use of hardware acceleration, the program is significantly slower than most others.

The most interesting part of the project is probably the mathematical [expression parser](src/expression/Expression.java), which is sensitive to operator precedence and parenthesis. I also found it difficult to automatically [draw the elements](src/grapher/Graph3D.java#L224) on the graph in the correct order. The current solution is mostly correct, although sometimes the edges of points can be blocked by objects behind them (only the point's very center is guaranteed to be drawn in the correct order).

It turns out that my algorithm for expression parsing is similar to the shunting-yard algorithm. However, I did not discover the latter until after completing most of this project.

### Running

Make sure Java 8 is installed. From the `bin/` directory, run `java grapher/Grapher`. To recompile, run `javac src/grapher/*.java src/expression/*.java -d bin` from the root of the repository.
