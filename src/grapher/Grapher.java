package grapher;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import expression.Expression;

/**
 * A simple, proof-of-concept 3D graphing program. Allows the user to enter a plain-text
 * mathematical expression using the operators <pre>+</pre>, <pre>-</pre>, <pre>*</pre>,
 * <pre>/</pre>, and <pre>^</pre>, along with several common functions. The expression is
 * graphed approximately by dividing the plane into small triangles and evaluating the
 * function at each vertex. In addition to graphing a function, the user may enter a
 * number of tuples <pre>(x, y, z)</pre> and the corresponding points will be shown on the
 * graph. The user can drag the mouse to rotate the graph, and use the scroll wheel to
 * zoom in or out.
 *
 * However, there is currently no way to move the frame away from the origin. Further,
 * since the graphics computation is written from scratch and does not make use of
 * hardware acceleration, the program is significantly slower than most others.
 *
 * The most interesting part of the project is probably the mathematical expression
 * parser, which is sensitive to operator precedence and parenthesis. I also found it
 * difficult to automatically draw the elements on the graph in the correct order. The
 * current solution is mostly correct, although sometimes the edges of points can be
 * blocked by objects behind them (only the point's very center is guaranteed to be drawn
 * in the correct order).
 *
 * @see Graph3D
 * @see expression.Expression
 * @author Everett Cheng
 */
public class Grapher {
    private static final Color DEFAULT_BACKGROUND = Color.WHITE;
    private static final Color DEFAULT_POINT_COLOR = Color.BLUE;
    private static final Color DEFAULT_AXIS_COLOR = Color.BLACK;

    private final JFrame window;
    private final Graph3D graph;
    private final JTextField eqInput;
    private final JTextField resInput;
    private final JLabel errLabel;

    /**
     * Create and show a new Grapher window, using the specified colors for the
     * background, points and axes.
     */
    public Grapher(final Color bgColor, final Color pointColor, final Color axisColor) {
        // initialize Grapher window
        window = new JFrame("3D Grapher");
        window.setPreferredSize(new Dimension(600, 698));
        window.setBackground(bgColor);
        window.setResizable(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // create Graph3D object
        graph = new Graph3D(bgColor, pointColor, axisColor);

        // initialize controls panel
        final GridBagLayout gbl = new GridBagLayout();
        final GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        final JPanel controls = new JPanel(gbl);
        controls.setPreferredSize(new Dimension(controls.getWidth(), 70));
        controls.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.BLACK));

        // initialize "z =" label and add it to controls panel
        final JLabel zLabel = new JLabel("z = ");
        zLabel.setFont(new Font("Serif", Font.PLAIN, 16));
        c.insets = new Insets(0, 10, 0, 0);
        c.weightx = 0.0;
        gbl.setConstraints(zLabel, c);
        controls.add(zLabel);

        // initialize equation input field and add it to controls panel
        eqInput = new JTextField();
        eqInput.setPreferredSize(new Dimension(0, 29));
        eqInput.setFont(new Font("Serif", Font.PLAIN, 15));
        c.insets = new Insets(0, 0, 0, 0);
        c.weightx = 4.0;
        gbl.setConstraints(eqInput, c);
        controls.add(eqInput);

        // initialize submit button and add it to controls panel
        final JButton updateButton = new JButton("Graph");
        updateButton.setPreferredSize(new Dimension(130, 30));
        c.insets = new Insets(0, 5, 0, 10);
        c.gridwidth = 3;
        c.weightx = 0.0;
        gbl.setConstraints(updateButton, c);
        controls.add(updateButton);

        c.gridy = 1;
        c.gridwidth = 1;
        // initialize error label and add it to second row of controls panel; this is used
        // to display error messages from attempts to parse user-entered functions
        errLabel = new JLabel("");
        errLabel.setFont(new Font("Serif", Font.PLAIN, 14));
        errLabel.setForeground(Color.RED);
        c.insets = new Insets(0, 5, 0, 0);
        c.weightx = 4.0;
        c.gridx = 1;
        gbl.setConstraints(errLabel, c);
        controls.add(errLabel);

        // initialize "Resolution:" label and add it to second row of controls panel
        final JLabel resLabel = new JLabel("Resolution:");
        resLabel.setFont(new Font("Serif", Font.PLAIN, 14));
        c.insets = new Insets(0, 15, 0, 0);
        c.weightx = 0.0;
        c.gridx = 2;
        gbl.setConstraints(resLabel, c);
        controls.add(resLabel);

        // initialize resolution input field and add it
        resInput = new JTextField();
        resInput.setPreferredSize(new Dimension(40, 19));
        resInput.setMinimumSize(new Dimension(40, 19));
        resInput.setFont(new Font("Serif", Font.PLAIN, 13));
        resInput.setText("" + graph.getGraphGridSize());
        resInput.setHorizontalAlignment(SwingConstants.RIGHT);
        c.insets = new Insets(0, 2, 0, 15);
        c.gridx = 4;
        gbl.setConstraints(resInput, c);
        controls.add(resInput);

        // set up listener to update the graph when the submit button is pressed
        final ActionListener eqnSubmitter = new EqnSubmitter();
        eqInput.addActionListener(eqnSubmitter); // enter key pressed
        updateButton.addActionListener(eqnSubmitter); // button clicked

        // set up listener to rotate the graph when the mouse is dragged
        graph.addMouseMotionListener(new RotationListener());

        // set up listener to zoom the graph with the scroll wheel
        graph.addMouseWheelListener(new ZoomListener());

        // set up listeners to change the resolution when resInput is submitted or loses
        // focus
        resInput.addActionListener(ae -> updateResolution());
        resInput.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(final FocusEvent fe) { updateResolution(); }
        });

        // populate main window
        window.add(graph, BorderLayout.CENTER);
        window.add(controls, BorderLayout.SOUTH);
        window.setLocationByPlatform(true);
        window.pack();
        window.validate();

        window.setVisible(true);
    }

    // validate and possibly apply the resolution entered by the user
    private void updateResolution() {
        try {
            final int newRes = Integer.valueOf(resInput.getText());
            // prevent unreasonable resolutions
            graph.setGraphGridSize(Math.min(Math.max(newRes, 5), 100));
            graph.repaint();
        } catch (final NumberFormatException e) { /* ignore malformed input */ }
        resInput.setText("" + graph.getGraphGridSize());
    }

    private class EqnSubmitter implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent ae) {
            errLabel.setText("");
            errLabel.setToolTipText("");
            updateResolution();

            String text = eqInput.getText().trim().toLowerCase();
            if (text.isEmpty()) return;

            // process view instructions: rotation and zoom, specified by certain letters
            switch (text) {
                case "l": case "left": // move camera leftwards
                    int inv = graph.camInverted() ? -1 : 1;
                    graph.updateCameraRot(0, -inv * Math.PI / 32);
                    return;
                case "r": case "right": // move camera rightwards
                    inv = graph.camInverted() ? -1 : 1;
                    graph.updateCameraRot(0, inv * Math.PI / 32);
                    return;
                case "u": case "up": // move camera upwards
                    graph.updateCameraRot(-Math.PI / 32, 0);
                    return;
                case "d": case "down": // move camera downwards
                    graph.updateCameraRot(Math.PI / 32, 0);
                    return;
                case "i": case "in": // zoom in
                    graph.updateCameraDistance(10.0 / 11.0);
                    return;
                case "o": case "out": // zoom out
                    graph.updateCameraDistance(11.0 / 10.0);
                    return;
                case "c": case "clear": // clear graph
                    graph.clearPoints();
                    graph.setFunction(null);
                    return;
            }

            // check if the user entered a point of the form "(x,y,z)" or "x,y,z"
            // if so, add the point to the graph
            if (text.startsWith("(") && text.endsWith(")"))
                text = text.substring(1, text.length()-1);
            final String[] coords = text.split("\\s*,\\s*");
            if (coords.length == 3) {
                try {
                    final Vector3D point = new Vector3D(Double.valueOf(coords[0]),
                                                        Double.valueOf(coords[1]),
                                                        Double.valueOf(coords[2]));
                    graph.addPoint(point);
                    return;
                } catch (final NumberFormatException e) { /* invalid point; ignore */ }
            }

            // attempt to parse as Expression
            try {
                graph.setFunction(Expression.getExpression(text));
            } catch (final IllegalArgumentException e) {
                errLabel.setText(e.getMessage());
                errLabel.setToolTipText(e.getMessage());
                window.repaint();
            }
        }
    }

    private class RotationListener implements MouseMotionListener {
        private int prevX, prevY;

        @Override
        public void mouseMoved(final MouseEvent e) {
            prevX = e.getX();
            prevY = e.getY();
        }

        @Override
        public void mouseDragged(final MouseEvent e) {
            final int dX = e.getX() - prevX;
            final int dY = e.getY() - prevY;
            prevX += dX;
            prevY += dY;
            // dragging the mouse 628 pixels rotates in a full circle.
            // this feels about right given the default size of the graph window
            graph.updateCameraRot(-dY / 100.0, -dX / 100.0);
        }
    }

    private class ZoomListener implements MouseWheelListener {
        @Override
        public void mouseWheelMoved(final MouseWheelEvent e) {
            final int n = e.getWheelRotation();
            final double scale = Math.pow(1.1, n);
            graph.updateCameraDistance(scale);
        }
    }

    public static void main(final String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Grapher(DEFAULT_BACKGROUND, DEFAULT_POINT_COLOR, DEFAULT_AXIS_COLOR);
            }
        });
    }
}
