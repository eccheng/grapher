package grapher;

/**
 * A simple immutable 3-dimensional vector class.
 *
 * @author Everett Cheng
 */
public class Vector3D {
    public final double x, y, z;

    public Vector3D(final double x, final double y, final double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }

    /**
     * Scale this vector by a constant scalar and return the resulting vector.
     */
    public Vector3D scale(final double scalar) {
        return new Vector3D(scalar * x, scalar * y, scalar * z);
    }

    public Vector3D minus(final Vector3D sub) {
        return new Vector3D(x-sub.x, y-sub.y, z-sub.z);
    }

    /**
     * Perform basic left-multiplication by a 3x3 matrix A, and return the resulting
     * vector.
     *
     * @param A
     *            A 3x3 array of representing a matrix.
     */
    // I have not used a matrix library because it would bring more complexity into this
    // proof-of-concept project.
    public Vector3D transform(final double[][] A) {
        if (A.length != 3 || A[0].length != 3 || A[1].length != 3 || A[2].length != 3)
            throw new IllegalArgumentException("Matrix must be 3x3.");
        return new Vector3D ( A[0][0]*x + A[0][1]*y + A[0][2]*z,
                              A[1][0]*x + A[1][1]*y + A[1][2]*z,
                              A[2][0]*x + A[2][1]*y + A[2][2]*z );
    }

    public final boolean isFinite() {
        return Double.isFinite(x) && Double.isFinite(y) && Double.isFinite(z);
    }
}
