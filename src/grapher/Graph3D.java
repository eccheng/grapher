package grapher;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MultipleGradientPaint.CycleMethod;
import java.awt.RadialGradientPaint;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.util.*;

import javax.swing.JComponent;

import expression.Expression;

/**
 * A JComponent which shows a 3D graph.
 *
 * @author Everett Cheng
 */
/*
 * Throughout this file, there are four main types of coordinates.
 *
 *    1. Graph coordinates: The theoretical x, y, and z coordinates of a point with
 *                          respect to the origin. The graph coordinates satisfy
 *                          z = f(x, y), where f is the function being graphed. Usually
 *                          represented by a Vector3D object, or just by the identifiers
 *                          x, y, and z.
 *
 *    2. Box coordinates: The two integer coordinates which identify a square in the
 *                        graph's xy-plane. There are graphGridSize * graphGridSize boxes,
 *                        and each box coordinate lies in the range
 *                        [-graphGridSize, graphGridSize). Usually represented by the
 *                        variables boxX and boxY.
 *
 *    3. Camera coordinates: Same as graph coordinates, except the coordinate system is
 *                           translated and rotated so that the camera faces in the
 *                           positive y-direction. Positive camera x is to the camera's
 *                           right, and positive camera z is above the camera. These are
 *                           never passed between methods, but are used as an intermediate
 *                           step in the project method.
 *
 *    4. Screen coordinates: Swing's standard screen coordinates, usually represented by
 *                           screenX and screenY.
 */
public class Graph3D extends JComponent {
    private static final long serialVersionUID = 1L;

    private static final double DEFAULT_CAMERA_DIST = 6 * Math.sqrt(3);
    private static final int DEFAULT_GRID_SIZE = 20;
    private static final double[] DEFAULT_CAMERA_ROT = { -Math.PI / 4, 3 * Math.PI / 4 };

    private final Color pointColor;
    private final Color axisColor;
    private final Color bgColor;

    // camera position (x, y, z) with respect to the graph
    private Vector3D camera;
    // counterclockwise camera rotations about x and z axes (we use the convention that
    // the camera starts facing in the positive y-direction, and is rotated about the
    // x-axis and then about the z-axis)
    private final double[] cameraRot;
    // distance from the origin; this is maintained while moving the camera right, left,
    // up, and down
    private double cameraDistance;
    // distance, in graph coordinates, from the origin to the edge of the graph along one
    // of the axes
    private double frameSize;
    // graph resolution (four triangles are painted for each box in the grid, and this
    // variable represents the number of grid boxes from the origin to the edge along one
    // of the axes)
    private int graphGridSize;

    // points explicitly entered by the user
    private final Set<Vector3D> points;

    // user-entered expression which is being graphed
    private Expression function;

    /**
     * Construct a new 3D graph with default view settings.
     */
    public Graph3D(final Color bg_col, final Color point_col, final Color axis_col) {
        bgColor = bg_col;
        pointColor = point_col;
        axisColor = axis_col;

        // initialize graph parameters
        cameraDistance = DEFAULT_CAMERA_DIST;
        graphGridSize = DEFAULT_GRID_SIZE;
        points = new HashSet<Vector3D>();
        cameraRot = new double[2];
        setCameraRot(DEFAULT_CAMERA_ROT[0], DEFAULT_CAMERA_ROT[1]);
    }

    /**
     * Construct a new 3D graph and display the given function.
     *
     * @param func
     *            An Expression object which depends on the variables <pre>x</pre> and
     *            <pre>y</pre>. Its values represent the height of the graph at various
     *            points.
     */
    public Graph3D(final Color bg_col, final Color point_col, final Color axis_col,
                   final Expression func) {
        this(bg_col, point_col, axis_col);
        setFunction(func);
    }

    /**
     * Add a new point to be displayed on the graph.
     *
     * @param point
     *            A Vector3D representing the point.
     */
    public void addPoint(final Vector3D point) {
        points.add(point);
        repaint();
    }

    /**
     * Clear all points on the graph.
     */
    public void clearPoints() {
        points.clear();
        repaint();
    }

    /**
     * Return the current size of the graph grid, i.e., the graph's resolution.
     */
    public int getGraphGridSize() {
        return graphGridSize;
    }

    /**
     * Set the graph's grid size or its resolution. Higher numbers cause the graph to
     * better approximate the true function, but slow down the program.
     */
    public void setGraphGridSize(final int size) {
        graphGridSize = size;
        repaint();
    }

    /**
     * Set the function to graph.
     * @param func
     *            An Expression object which depends on the variables <pre>x</pre> and
     *            <pre>y</pre>. Its values represent the height of the graph at various
     *            points.
     */
    public void setFunction(final Expression func) {
        function = func;
        repaint();
    }

    /**
     * Set the camera angle. If rotX and rotZ are both zero, the camera faces in the
     * positive y-direction. Starting from here, the camera faces in the direction
     * obtained by rotating it counterclockwise by rotX radians about the positive x-axis,
     * and then rotating it counterclockwise by rotZ radians about the positive z-axis.
     *
     * In spherical coordinates, the camera will face in the direction given by
     * <pre>phi = pi/2 - rotX</pre> and <pre>theta = pi/2 + rotZ</pre>. The camera always
     * faces towards the origin.
     */
    public void setCameraRot(final double rotX, final double rotZ) {
        cameraRot[0] = rotX;
        cameraRot[1] = rotZ;
        final Vector3D camface = new Vector3D(
            -Math.cos(rotX) * Math.sin(rotZ),
             Math.cos(rotX) * Math.cos(rotZ),
             Math.sin(rotX)
        );
        camera = camface.scale(-cameraDistance);
        // set frameSize so that the frame reaches at most halfway to the camera
        frameSize = cameraDistance / (2 * Math.sqrt(3));
        repaint();
    }

    /**
     * Set the camera angle relative to the current one.
     *
     * @param drotX
     *            The change in rotX, as defined by the setCameraRot method.
     * @param drotZ
     *            The change in rotZ, as defined by the setCameraRot method.
     */
    public void updateCameraRot(final double drotX, final double drotZ) {
        setCameraRot(cameraRot[0] + drotX, cameraRot[1] + drotZ);
    }

    /**
     * Scalar the camera distance by a set factor, without affecting the camera angle.
     *
     * @param mult
     *            A positive number used to multiply the camera distance.
     */
    public void updateCameraDistance(final double mult) {
        final double newDist = cameraDistance * mult;
        if (newDist > 0) {
            cameraDistance = newDist;
            // update camera position and frameSize
            updateCameraRot(0, 0);
        }
    }

    /**
     * When the camera crosses past vertical, the directions "right" and "left" affect
     * begin to affect rotX in the opposite way. This method determines whether "right"
     * means increasing rotX or decreasing it.
     *
     * This is used when the user enters "right" or "left" into the equation box to turn
     * the graph.
     */
    public boolean camInverted() {
        final double offset = Math.abs(cameraRot[0]) % (2 * Math.PI);
        return (offset > Math.PI / 2 && offset < (3 * Math.PI) / 2);
    }

    @Override
    public void paintComponent(final Graphics g) {
        super.paintComponent(g);
        final Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setBackground(bgColor);
        g2.clearRect(0, 0, getWidth(), getHeight());

        // find axis locations
        final double[] origin = project(new Vector3D(0, 0, 0));
        final double[][] axes = {
            project(new Vector3D(frameSize, 0, 0)), // x-axis endpoint
            project(new Vector3D(0, frameSize, 0)), // y-axis endpoint
            project(new Vector3D(0, 0, frameSize)), // z-axis endpoint
        };

        // draw the function graph and axes
        final boolean right = camera.x > 0;
        final boolean upper = camera.y > 0;
        drawQuadrant(g2, !right, !upper); // opposite quadrant
        if (!right) drawAxis(g2, origin, axes[0]); // draw axes which are far from camera
        if (!upper) drawAxis(g2, origin, axes[1]);
        drawQuadrant(g2, !right,  upper); // adjacent quadrant
        drawQuadrant(g2,  right, !upper); // other adjacent quadrant
        if (right) drawAxis(g2, origin, axes[0]); // draw axes near camera
        if (upper) drawAxis(g2, origin, axes[1]);
        drawAxis(g2, origin, axes[2]);
        drawQuadrant(g2,  right,  upper); // near quadrant
    }

    /*
     * Draw a line to represent one of the axes.
     */
    private void drawAxis(final Graphics2D g2, final double[] orig, final double[] end) {
        g2.setColor(axisColor);
        g2.drawLine((int) orig[1], (int) orig[2], (int) end[1], (int) end[2]);
    }

    /*
     * Accepts a point specified in graph coordinates, and converts it to screen
     * coordinates. If the point is behind the camera plane, returns null. Also returns
     * the radius of the circle to draw for a user-specified point, since that radius
     * depends on the point's distance from the camera.
     */
    private double[] project(final Vector3D point) {
        // convert to camera coordinates, by performing the inverse rotations from the
        // usual rotX-then-rotZ
        final Vector3D diff = point.minus(camera);

        // rotates clockwise about the z-axis by cameraRot[1]
        final double[][] rotZ = {{  Math.cos(cameraRot[1]), Math.sin(cameraRot[1]), 0 },
                                 { -Math.sin(cameraRot[1]), Math.cos(cameraRot[1]), 0 },
                                 {            0,                      0,            1 }};

        // rotates clockwise about the x-axis by cameraRot[0]
        final double[][] rotX = {{ 1,            0,                      0            },
                                 { 0,  Math.cos(cameraRot[0]), Math.sin(cameraRot[0]) },
                                 { 0, -Math.sin(cameraRot[0]), Math.cos(cameraRot[0]) }};

        final Vector3D relative = diff.transform(rotZ).transform(rotX); // camera coords
        if (relative.y <= 0)
            return null;
        // Here, 750 is another arbitrary constant specifying how large the graph should
        // appear on the screen.
        final double radius = 37.5 / relative.y;
        final double screenX = getWidth() / 2 + (relative.x * 750) / relative.y;
        final double screenY = getHeight() / 2 - (relative.z * 750) / relative.y;
        return new double[] { radius, screenX, screenY };
    }

    private void drawTriangle(final Graphics2D g2, final Vector3D[] tri,
                              final double hue) {
        if (!(inGraph(tri[0]) || inGraph(tri[1]) || inGraph(tri[2])))
            return;
        if (!tri[0].isFinite() || !tri[1].isFinite() || !tri[2].isFinite())
            return;

        final double[] p0 = project(tri[0]);
        final double[] p1 = project(tri[1]);
        final double[] p2 = project(tri[2]);
        if (p0 == null || p1 == null || p2 == null)
            return;

        g2.setColor(Color.getHSBColor((float) hue, 1f, 1f));
        final Path2D.Double tr = new Path2D.Double();
        tr.moveTo(p0[1], p0[2]);
        tr.lineTo(p1[1], p1[2]);
        tr.lineTo(p2[1], p2[2]);
        tr.lineTo(p0[1], p0[2]);
        g2.fill(tr);
    }

    // test if point is within frame
    private boolean inGraph(final Vector3D point) {
        return ( ((Double) Math.abs(point.x)).compareTo(frameSize) <= 0
              && ((Double) Math.abs(point.y)).compareTo(frameSize) <= 0
              && ((Double) Math.abs(point.z)).compareTo(frameSize) <= 0);
    }

    /*
     * Draw all of the boxes in a single quadrant of the graph. The boxes can be drawn in
     * a number of correct orders; here, we just draw the columns far from the camera
     * before those near the camera, and the same goes for the boxes within the columns.
     */
    private void drawQuadrant(final Graphics2D g2, final boolean right,
                                                   final boolean upper) {
        // Minimum box coordinates for this quadrant. The maximum coordinates will be
        // minX + graphGridSize - 1  and  minY + graphGridSize - 1  .
        final int minX = right ? 0 : -graphGridSize;
        final int minY = upper ? 0 : -graphGridSize;

        final int[] camBox = getBox(camera.x, camera.y);
        // draw columns with lower x-coordinate than camera box
        for (int boxX = minX; boxX < camBox[0] && boxX < minX + graphGridSize; ++boxX)
            drawQuadrantColumn(g2, boxX, minY, camBox[1]);

        // draw columns with higher x-coordinate than camera box (or equal)
        for (int boxX = minX + graphGridSize-1; boxX >= camBox[0] && boxX >= minX; --boxX)
            drawQuadrantColumn(g2, boxX, minY, camBox[1]);
    }

    /*
     * Draw half of a single column (fixed boxX coordinate).
     */
    private void drawQuadrantColumn(final Graphics2D g2, final int colX,
                                    final int minY, final int camY) {
        // draw rows with lower y-coordinate than camera box
        for (int boxY = minY; boxY < camY && boxY < minY + graphGridSize; ++boxY)
            drawBox(g2, colX, boxY);
        // draw rows with higher y-coordinate than camera box (or equal)
        for (int boxY = minY + graphGridSize-1; boxY >= camY && boxY >= minY; --boxY)
            drawBox(g2, colX, boxY);
    }

    /*
     * Accepts the box coordinates of a box, and draws the function on the box, along with
     * any user-specified points that lie above the box.
     *
     * For a box (boxX, boxY), the corner with lower x and y has graph coordinates
     * (x0, y0) = (boxX * gridStep, boxY * gridStep) , where each box has width and height
     * equal to gridStep.
     *
     * For naming the corners, let x1 = x0 + gridStep and y1 = y0 + gridStep. Then the
     * corners are c00 = (x0, y0) , c10 = (x1, y0) , c01 = (x0, y1) , c11 = (x1, y1) .
     */
    private void drawBox(final Graphics2D g2, final int boxX, final int boxY) {
        final double gridStep = frameSize / graphGridSize;
        final double x0 = boxX * gridStep;
        final double y0 = boxY * gridStep;
        // compute function at corners and center of box
        final Map<String, Double> vars = new HashMap<String, Double>(2);
        final Vector3D c00 = graphPoint(vars, x0,            y0           );
        final Vector3D c10 = graphPoint(vars, x0 + gridStep, y0           );
        final Vector3D c01 = graphPoint(vars, x0,            y0 + gridStep);
        final Vector3D c11 = graphPoint(vars, x0 + gridStep, y0 + gridStep);
        final Vector3D center = graphPoint(vars, x0 + gridStep/2, y0 + gridStep/2);

        /*
         * Now, divide the box into four triangles:
         *     ___
         *    |\ /|      3
         *    | X |    2   1
         *    |/_\|      0
         */
        final Vector3D[][] triangles = {
                { c00, c10, center }, // 0
                { c10, c11, center }, // 1
                { c00, c01, center }, // 2
                { c01, c11, center }, // 3
        };

        /*
         * By extending the diagonals of the box, (the slashes in the figure above)
         * we divide the plane into four regions. The camera is closest to the
         * triangle which lies in the same region as it. This triangle is drawn last,
         * and the opposite triangle is drawn first.
         */
        final int[][] orders = {
            { 3, 2, 1, 0 }, // camera in region 0
            { 2, 3, 0, 1 }, // camera in region 1
            { 1, 0, 3, 2 }, // camera in region 2
            { 0, 1, 2, 3 }, // camera in region 3
        };
        // select the right order
        final int[] order = orders[closestTriangle(camera, boxX, boxY)];

        // draw the triangles and points
        for (int i = 0; i < 4; ++i) {
            final Vector3D[] tri = triangles[order[i]];

            final List<double[]> pointsBehind = new ArrayList<double[]>();
            final List<double[]> pointsBefore = new ArrayList<double[]>();
            if (!points.isEmpty()) {
                /*
                 * With each triangle, we want to draw any points that lie directly above
                 * or below. We first extend the plane defined by the points in tri: this
                 * can be given by
                 *     a(x-center.x) + b(y-center.y) + center.z - z = 0
                 * for some a and b. If the camera and the point lie on the same side of
                 * the plane, the point should be drawn in front of the triangle;
                 * otherwise, it should go behind it.
                 *
                 * Note that a and b are the slopes of the plane in the x and y
                 * directions. The quantity (tri[1].z-tri[0].z)/gridStep equals a when
                 * order[i] is 0 or 3, and equals b when order[i] is 1 or 2.
                 * The quantity (2*center.z-tri[0].z-tri[1].z)/gridStep equals the other
                 * slope, but needs to be negated when order[i] is odd.
                 */
                // compute a and b
                final double slope1 = (tri[1].z - tri[0].z) / gridStep;
                double slope2 = (2 * center.z - tri[0].z - tri[1].z) / gridStep;
                if (order[i] % 2 == 0) slope2 *= -1;
                final double a = (order[i] == 0 || order[i] == 3) ? slope1 : slope2;
                final double b = (order[i] == 0 || order[i] == 3) ? slope2 : slope1;

                // populate pointsBehind and pointsBefore
                for (final Vector3D p : points) {
                    // ignore points which are not above or below this triangle
                    final int[] pbox = getBox(p.x, p.y);
                    if (pbox[0] != boxX || pbox[1] != boxY) continue;
                    if (closestTriangle(p, boxX, boxY) != order[i]) continue;

                    // is the point on the same side of the plane as the camera?
                    final double pMargin = a*(p.x-center.x) + b*(p.y-center.y)
                                            + center.z - p.z;
                    final double camMargin = a*(camera.x-center.x) + b*(camera.y-center.y)
                                              + center.z - camera.z;
                    final boolean isBefore = pMargin * camMargin >= 0;
                    // get coordinates for where to draw the point p, and for the radius
                    // of its dot on the screen; add these numbers to the correct list
                    final double[] proj = project(p);
                    if (proj != null)
                        (isBefore ? pointsBefore : pointsBehind).add(proj);
                }
            }

            // Draw the points which are behind the triangle; smaller radii first, since
            // they are deeper in the camera's field of view.
            pointsBehind.sort((p1, p2) -> (int) Math.signum(p1[0] - p2[0]));
            for (final double[] dot : pointsBehind)
                drawPointOnScreen(g2, dot[0], dot[1], dot[2]);

            /*
             * Draw the triangle.
             * We color by mean height: the mean value of an affine function over a
             * 45-45-90 triangle is just a weighted average of the values at the
             * vertices, with the vertex opposite the hypotenuse weighted four times
             * as heavily as the other two.
             */
            final double meanHeight = (tri[0].z + 4*tri[1].z + tri[2].z) / 6;
            final double hue = (meanHeight / frameSize + 1) % 1;
            drawTriangle(g2, tri, hue);

            // Draw the points which are before the triangle, smaller radii first.
            pointsBefore.sort((p1, p2) -> (int) Math.signum(p1[0] - p2[0]));
            for (final double[] dot : pointsBefore)
                drawPointOnScreen(g2, dot[0], dot[1], dot[2]);
        }
    }

    // Find the point on the function's graph which has the given x and y.
    private Vector3D graphPoint(final Map<String, Double> vars, final double x,
                                                                final double y) {
        vars.put("x", x);
        vars.put("y", y);
        return new Vector3D(x,y, function != null ? function.evaluate(vars) : Double.NaN);
    }

    // Fill a nice circular dot to represent one of the user-specified points.
    private void drawPointOnScreen(final Graphics2D g2, final double radius,
                                   final double screenX, final double screenY) {
        final Point2D center = new Point2D.Double(screenX, screenY);
        final Point2D focus = new Point2D.Double(screenX - radius/2, screenY - radius/2);
        final float[] dist = { 0.0f, 0.6f };
        final Color[] colors = { bgColor, pointColor };
        g2.setPaint(new RadialGradientPaint(center, (float) radius, focus, dist, colors,
                    CycleMethod.NO_CYCLE));
        g2.fill(new Ellipse2D.Double(screenX - radius, screenY - radius,
                                     2*radius, 2*radius));
    }

    /*
     * Return the position of the box of the graph grid which contains the points with
     * the given x and y. The results are given in box coordinates, and so fall in the
     * range [-graphGridSize, graphGridSize) .
     */
    private int[] getBox(final double x, final double y) {
        int boxX = (int) (x * graphGridSize / frameSize);
        int boxY = (int) (y * graphGridSize / frameSize);
        // minimum and maximum box coordinates; inclusive
        final int minBox = -graphGridSize;
        final int maxBox = graphGridSize - 1;
        boxX = Math.min(Math.max(boxX, minBox), maxBox);
        boxY = Math.min(Math.max(boxY, minBox), maxBox);
        return new int[] { boxX, boxY };
    }

    // Divides the plane into the four regions used by the drawBox method above, and
    // returns the region in which the given point lies.
    private int closestTriangle(final Vector3D point, final int boxX, final int boxY) {
        final double centerX = (frameSize / graphGridSize) * (boxX + 0.5);
        final double centerY = (frameSize / graphGridSize) * (boxY + 0.5);
        final double relX = point.x - centerX;
        final double relY = point.y - centerY;
        if (relY < relX) {
            if (relY < -relX)
                return 0;
            return 1;
        }
        if (relY < -relX)
            return 2;
        return 3;
    }
}
