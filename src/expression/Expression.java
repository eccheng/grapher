package expression;

import java.util.*;

/**
 * A mathematical expression which depends on a number of Double-valued variables, and
 * which can be evaluated to produce a single Double.
 *
 * @author Everett Cheng
 */
/*
 * Expressions are represented as trees. Each Expression has a specific mathematical
 * operation or function, along with any number of subexpressions. In order to evaluate
 * the Expression, its operation is applied to the results of evaluating its
 * subexpressions.
 */
public class Expression {
    private static interface Operation {
        public double apply(double[] vals);
    }

    // represents either an infix mathematical operator, or a mathematical function
    private static class Operator {
        final String name;
        final Operation operation;
        final boolean infix;
        final int arity;
        final int precedence;

        Operator(final String name, final Operation operation, final boolean infix,
                 final int arity, final int precedence) {
            if (infix && arity != 2)
                throw new IllegalArgumentException("Infix operators must have arity 2.");
            if (!infix && arity != 1)
                throw new IllegalArgumentException("Prefix/function operators with "
                                + "multiple arguments are not currently supported.");
            this.name = name;
            this.operation = operation;
            this.infix = infix;
            this.arity = arity;
            this.precedence = precedence;
        }
    }

    // table of supported operators; see static block below
    private static final Map<String, Operator> OPERATORS = new HashMap<String, Operator>();

    private final Operator operator;
    private final Expression[] subs;

    private Expression(final Operator optr, final Expression... subs) {
        operator = optr;
        this.subs = subs;
    }

    /**
     * Evaluates this Expression given a certain assignment of variables to values.
     *
     * @param vars
     *            A map associating any number of single-letter variable names with
     *            numerical values.
     */
    public double evaluate(final Map<String, Double> vars) {
        final double[] vals = new double[subs.length];
        for (int i = 0; i < subs.length; ++i)
            vals[i] = subs[i].evaluate(vars);
        return operator.operation.apply(vals);
    }

    @Override
    public String toString() {
        return operator.name + " " + Arrays.toString(subs);
    }

    // return Expression consisting of a single constant
    private static final Expression getConstant(final Double val) {
        return new Expression(null) {
            @Override
            public double evaluate(final Map<String, Double> vars) {
                return val;
            }
            @Override
            public String toString() {
                return val.toString();
            }
        };
    }

    // return Expression consisting of a single variable name
    private static final Expression getVariable(final String var) {
        return new Expression(null) {
            @Override
            public double evaluate(final Map<String, Double> vars) {
                if (!vars.containsKey(var))
                    error("unknown variable " + var + ".");
                return vars.get(var);
            }
            @Override
            public String toString() {
                return var;
            }
        };
    }

    private static void addOp(final String name, final int arity, final boolean infix,
                              final int prec, final Operation op) {
        OPERATORS.put(name, new Operator(name, op, infix, arity, prec));
    }

    // operator definitions
    static {
        // arithmetic operators
        addOp("+", 2, true, 1, v -> v[0] + v[1]);
        addOp("-", 2, true, 1, v -> v[0] - v[1]);
        addOp("*", 2, true, 3, v -> v[0] * v[1]);
        addOp("/", 2, true, 3, v -> v[0] * Math.pow(v[1], -1));
        addOp("^", 2, true, 5, v -> Math.pow(v[0], v[1]));
        // absolute value; square root; exponential and logarithm
        addOp("abs", 1, false, 4, v -> Math.abs(v[0]));
        addOp("sqrt", 1, false, 4, v -> Math.sqrt(v[0]));
        addOp("exp", 1, false, 4, v -> Math.exp(v[0]));
        addOp("log", 1, false, 4, v -> Math.log(v[0]));
        addOp("ln", 1, false, 4, v -> Math.log(v[0]));
        // rounding functions; sign function
        addOp("ceil", 1, false, 4, v -> Math.ceil(v[0]));
        addOp("floor", 1, false, 4, v -> Math.floor(v[0]));
        addOp("round", 1, false, 4, v -> Math.round(v[0]));
        addOp("sgn", 1, false, 4, v -> Math.signum(v[0]));

        // standard and hyperbolic trig functions
        addOp("sin", 1, false, 4, v -> Math.sin(v[0]));
        addOp("cos", 1, false, 4, v -> Math.cos(v[0]));
        addOp("tan", 1, false, 4, v -> Math.tan(v[0]));
        addOp("sec", 1, false, 4, v -> Math.pow(Math.cos(v[0]), -1));
        addOp("csc", 1, false, 4, v -> Math.pow(Math.sin(v[0]), -1));
        addOp("cot", 1, false, 4, v -> Math.pow(Math.tan(v[0]), -1));
        addOp("sinh", 1, false, 4, v -> Math.sinh(v[0]));
        addOp("cosh", 1, false, 4, v -> Math.cosh(v[0]));
        addOp("tanh", 1, false, 4, v -> Math.tanh(v[0]));
        addOp("sech", 1, false, 4, v -> Math.pow(Math.cosh(v[0]), -1));
        addOp("csch", 1, false, 4, v -> Math.pow(Math.sinh(v[0]), -1));
        addOp("coth", 1, false, 4, v -> Math.pow(Math.tanh(v[0]), -1));

        // inverse standard and inverse hyperbolic trig functions
        addOp("asin", 1, false, 4, v -> Math.asin(v[0]));
        addOp("acos", 1, false, 4, v -> Math.acos(v[0]));
        addOp("atan", 1, false, 4, v -> Math.atan(v[0]));
        addOp("asec", 1, false, 4, v -> Math.acos(Math.pow(v[0], -1)));
        addOp("acsc", 1, false, 4, v -> Math.asin(Math.pow(v[0], -1)));
        addOp("acot", 1, false, 4, v -> Math.atan(Math.pow(v[0], -1)));
        addOp("asinh", 1, false, 4, v -> Math.log(v[0] + Math.sqrt(v[0] * v[0] + 1)));
        addOp("acosh", 1, false, 4, v -> Math.log(v[0] + Math.sqrt(v[0] * v[0] - 1)));
        addOp("atanh", 1, false, 4, v ->
                    0.5 * Math.log((1 + v[0]) * Math.pow(1 - v[0], -1)));
        addOp("asech", 1, false, 4, v ->
                    Math.log(Math.pow(v[0], -1) + Math.sqrt(Math.pow(v[0], -2) - 1)));
        addOp("acsch", 1, false, 4, v ->
                    Math.log(Math.pow(v[0], -1) + Math.sqrt(Math.pow(v[0], -2) + 1)));
        addOp("acoth", 1, false, 4, v ->
                    0.5 * Math.log((1 + v[0]) * Math.pow(v[0] - 1, -1)));
    }

    // separate an input string into tokens and place them in a queue
    private static Queue<Object> tokenize(final String str) {
        // Valid tokens are either Operators, constant and variable Expressions, and
        // Strings from the DELIMITERS list below.
        final Queue<Object> tokens = new LinkedList<Object>();
        String chunk = "";
        for (int i = 0; i < str.length();) {
            // ignore whitespace
            if (Character.isWhitespace(str.charAt(i))) {
                ++i;
                continue;
            }
            // search for largest possible token starting at the current position
            boolean success = false;
            for (int j = str.length(); j > i; --j) {
                chunk = str.substring(i, j);
                // check for known token (operator name, comma, or bracket)
                final String[] DELIMITERS = { "(", ")", "[", "]", "|" };
                if (OPERATORS.containsKey(chunk)) {
                    tokens.add(OPERATORS.get(chunk));
                    success = true;
                    break;
                }
                if (Arrays.asList(DELIMITERS).contains(chunk)) {
                    tokens.add(chunk);
                    success = true;
                    break;
                }
                // check for number literal
                try {
                    final Double val = Double.valueOf(chunk);
                    tokens.add(Expression.getConstant(val));
                    success = true;
                    break;
                } catch (final NumberFormatException e) {}
            }
            if (success)
                i += chunk.length();
            else if (Character.isLetter(str.charAt(i))) {
                // we have a valid variable name (only 1-letter names allowed)
                tokens.add(Expression.getVariable(chunk));
                ++i;
            } else
                error("unrecognized token.", i, str);
        }
        return tokens;
    }

    /**
     * Accepts a String, and attempts to parse it into an Expression. The usual symbols
     * of arithmetic apply, and a number of common functions are available.
     *
     * @throws IllegalArgumentException
     *             when the input does not contain a valid expression.
     */
    /*
     * My algorithm is as follows:
     *
     * Prepare a stack for operators and open-brackets which have not yet been "resolved,"
     * i.e., the bracket is still unmatched, or the operator's operands have not yet been
     * determined and combined into a single Expression. Prepare a line of "cells" which
     * will contain Expressions which we are in the progress of building (this line of
     * cells is implemented as a list). We iterate through the tokens of the input string,
     * manipulating our cells and keeping track of our position in the line.
     *
     * If we read an Expression such as a number literal or variable, we add it to the
     * current cell, which should be empty. If we encounter an Operator, we resolve other
     * operators currently on the stack using the resolveOperators method, and then add
     * the new Operator to the stack. We also move to the next cell if the new operator is
     * infix, since we expect another expression to follow. If we encounter an open-
     * bracket, we simply push it to the stack. For a closing-bracket, we need to resolve
     * operators from the stack which originated from the subexpression we are trying to
     * close.
     */
    public static Expression getExpression(final String input)
                throws IllegalArgumentException {
        final Queue<Object> tokens = tokenize(input);
        final Stack<Object> symbols = new Stack<Object>();
        final List<Expression> expr = new ArrayList<Expression>();
        int pos = 0;
        for (final Object tok : tokens) {
            if (tok instanceof Operator) {
                final Operator op = (Operator) tok;
                if (op.infix)
                    pos = resolveOperators(symbols, expr, pos, op.precedence) + 1;
                symbols.push(op);
            } else if (tok.equals("(") || tok.equals("[")) {
                symbols.push(tok);
            } else if (tok.equals(")") || tok.equals("]")) {
                final String opener = tok.equals(")") ? "(" : "[";
                // resolve all hanging operators until finding opener
                pos = resolveOperators(symbols, expr, pos, -1);
                if (symbols.empty() || !symbols.pop().equals(opener))
                    error("unmatched " + tok + ".");
            } else if (tok.equals("|")) {
                // We first find out if this is a closing bar or an opening one;
                // for it to be a closer, the most recent opener on the stack should be
                // another pipe and not ( or [.
                boolean isCloser = false;
                final Stack<Object> displaced = new Stack<Object>();
                while (!symbols.empty() && symbols.peek() instanceof Operator)
                    displaced.push(symbols.pop());
                if (!symbols.isEmpty() && symbols.peek().equals("|"))
                    isCloser = true;
                // return displaced symbols to main stack
                while (!displaced.empty())
                    symbols.push(displaced.pop());
                if (isCloser) {
                    pos = resolveOperators(symbols, expr, pos, -1);
                    symbols.pop();
                    expr.set(pos, new Expression(OPERATORS.get("abs"), expr.get(pos)));
                } else
                    symbols.push("|");
            } else if (expr.size() > pos) { // consecutive expressions without operator
                error("expected an operator, but found token " + tok + ".");
            } else // token is a variable or a constant
                expr.add(pos, (Expression)tok);
        }
        pos = resolveOperators(symbols, expr, pos, -1);
        if (!symbols.empty()) // we have leftover open brackets
            error("unmatched " + symbols.peek() + ".");
        // there must be exactly one Expression in the list, which is our result
        return expr.get(0);
    }

    /*
     * Iteratively combine expressions in the nearby cells using operators from the stack
     * until we find a delimiter, or an operator with strictly lower precedence than the
     * current one given by prec. This may change the current cell, so we return our new
     * position.
     *
     * For example, if we started with the tokens a + b * c ^ d, we
     * might have the expressions a, b, c, d, in our list and operators +, *, ^ in our
     * stack. If we now read a "*" token, we first want to condense as follows:
     *
     *   EXPRESSION LIST              OPERATOR STACK (top-->)
     *     a, b,      c,  d             +, *, ^
     *     a, b,      c^d               +, *
     *     a, b*(c^d)                   +
     *
     * We do not resolve the "+" operator, since its precedence is lower than that of our
     * new operator "*". Future tokens could still be operands of the "+". Note that we
     * have moved two cells to the left.
     *
     * This method is mostly called upon reading a new operator from the input queue. If
     * we are not reading a new operator but are at the end of a subexpression, the
     * precedence check is unnecessary; we just want to resolve operators until finding
     * the start of the subexpression. In this case, passing -1 as the current precedence
     * achieves the desired result.
     */
    private static int resolveOperators(final Stack<Object> symbols,
                                   final List<Expression> expr, int pos, final int prec) {
        while (!symbols.empty() && symbols.peek() instanceof Operator) {
            final Operator op = (Operator) symbols.peek();
            if (op.precedence < prec)
                break;
            symbols.pop();
            try {
                // for now, only single-argument prefix operators are allowed
                if (op.arity == 2) {
                    expr.set(pos - 1,
                             new Expression(op, expr.get(pos - 1), expr.get(pos)));
                    // remove trailing cell which was used for the operator's second argument
                    expr.remove(pos);
                    --pos;
                } else
                    expr.set(pos, new Expression(op, expr.get(pos)));
            } catch (final IndexOutOfBoundsException e) {
                error("expected more arguments for operator " + op.name + ".");
            }
        }
        return pos;
    }

    private static void error(final String msg, final int index, final String input) {
        final String message = "Failed to parse input at column " + index + ": " + msg;
        System.err.print(message + "\n\n    " + input + "\n    ");
        for (int i = 0; i < index; ++i)
            System.err.print(" ");
        System.err.println("^");
        throw new IllegalArgumentException(message);
    }

    private static void error(final String msg) {
        final String message = "Failed to parse or evaluate input: " + msg;
        System.err.println(message);
        throw new IllegalArgumentException(message);
    }

    // used for testing expression parser
    public static void main(final String[] args) {
        System.out.print("Enter expression: ");

        final Scanner sc = new Scanner(System.in);
        final String str = sc.nextLine();
        final Expression expr = getExpression(str);
        System.out.println(expr);

        String query = "";
        while (!query.startsWith("q") && !query.startsWith("Q")) {
            System.out.println("Enter pairs of variables and values, one per line. "
                                + "Type 'go' to evaluate.");
            final Map<String, Double> vals = new HashMap<String, Double>();
            String inp = sc.nextLine();
            while (!inp.equals("go")) {
                vals.put(inp, Double.parseDouble(sc.nextLine()));
                inp = sc.nextLine();
            }
            System.out.println(expr.evaluate(vals));
            System.out.print("Press return to enter new values, or 'q' to quit: ");
            query = sc.nextLine();
        }
        sc.close();
    }
}
